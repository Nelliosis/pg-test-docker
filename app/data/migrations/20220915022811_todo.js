
exports.up = knex =>
    knex.schema.raw('CREATE EXTENSION IF NOT EXISTS pgcrypto')
        .createTable("cards", tbl => {
            tbl.increments("id");
            tbl.binary("name");
            tbl.jsonb("description");
        });


exports.down = knex => knex.schema.dropTableIfExists("cards");
