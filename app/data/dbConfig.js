const knex = require('knex');
const knexfile = require('../knexfile');

const env = process.env.NODE_ENV || 'development';
const dbConfig = knexfile[env];

module.exports = knex(dbConfig);
