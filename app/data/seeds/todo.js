const fs = require('fs');
const pubKey = fs.readFileSync('./keys/public.key').toString();


exports.seed = async function (knex) {

  await knex("cards").del()
  await knex("cards").insert(knex.raw(`(id, name, description) VALUES (DEFAULT, pgp_pub_encrypt(?, dearmor(?)), ?)`, ["Misty", pubKey, { title: "The first Girl" }]));
  await knex("cards").insert(knex.raw(`(id, name, description) VALUES (DEFAULT, pgp_pub_encrypt(?, dearmor(?)), ?)`, ["May", pubKey, { title: "The second Girl" }]));
  await knex("cards").insert(knex.raw(`(id, name, description) VALUES (DEFAULT, pgp_pub_encrypt(?, dearmor(?)), ?)`, ["Dawn", pubKey, { title: "The best Girl" }]));

  // await knex("cards").insert([
  //   { id: 1, name: "Misty", description: { title: "The first Girl" } },
  //   { id: 2, name: "May", description: { title: "The second Girl" } },
  //   { id: 3, name: "Dawn", description: { title: "The best Girl" } }
  // ]);
};
