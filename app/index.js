const koa = require('koa');
const Router = require('koa-router');
const jsonBody = require('koa-json-body');
const logger = require('koa-logger');

const swagger = require('swagger2');
const swagger2koa = require('swagger2-koa');

const { ui, validate } = swagger2koa;

const db = require('./model.js');

const spec = swagger.loadDocumentSync('./swagger.yaml');



if (!swagger.validateDocument(spec)) {
    throw Error('Swagger not valid')
}

const app = new koa().use(jsonBody())
const router = Router({ prefix: '/v1' });
const port = 8080;

router

    .post('/cards', async (context) => { //done
        const data = context.request.body;
        const id = await db.insert(data.name, data.description)

        context.status = 200;
        context.body = id;
    })

    .get('/cards', async (context) => { //done
        const data = await db.retrieveAll();

        context.status = 200;
        context.body = data;
    })

    .get('/cards/:id', async (context) => { //done
        const data = await db.retrieve(context.params.id);
        context.status = 200;

        if (data.hasOwnProperty('message')) {
            context.status = 404;
        }

        context.body = data;
    })

    .put('/cards/:id', async (context) => { //done
        const data = context.request.body;
        await db.update(data.name, context.params.id, data.description);

        context.status = 204;
    })

    .delete('/cards/:id', async (context) => {
        const data = context.params.id;
        await db.deleteCard(data);

        context.status = 204;
    })

router.get('/swagger.json', async (context) => {
    context.body = spec;
})

app.use(validate(spec));
app.use(ui(spec, '/', ['/v1']));
app.use(logger());
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(port, console.log(`go to http://localhost:${port}/docs`));